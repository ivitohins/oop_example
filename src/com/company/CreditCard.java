package com.company;

public class CreditCard extends AbstractCard {
    private float applyOperation(float charge){
        return charge + 0.50f;
    }
    @Override
    public void removeMoney(float amount) {
        float tempAmount;

        BankAccount tempAcc = getBankAccount();
        tempAmount = tempAcc.getAmount();
        tempAmount = (tempAmount - applyOperation(amount));
        tempAcc.setAmount(tempAmount);
    }
}

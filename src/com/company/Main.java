package com.company;

public class Main {

    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setOwner("Andris");
        bankAccount.setNum("12345678");
        bankAccount.setAmount(1000);
        System.out.println(bankAccount);

        CreditCard creditCard = new CreditCard();
        creditCard.setCardNum("122123");
        creditCard.setHolder("Alex");
        creditCard.setBankAccount(bankAccount);
        creditCard.putMoney(1000);
		System.out.println(bankAccount);
        creditCard.removeMoney(200);
		System.out.println(bankAccount);

        DebitCard debitCard = new DebitCard();
		debitCard.setCardNum("111111");
		debitCard.setHolder("Alex");
		debitCard.setBankAccount(bankAccount);
		debitCard.putMoney(90);
		System.out.println(bankAccount);
		debitCard.removeMoney(2);

		System.out.println(bankAccount);

    }
}

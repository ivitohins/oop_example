package com.company;

public class DebitCard extends AbstractCard {
    @Override
    public void removeMoney(float amount) {
        float tempAmount;

        BankAccount tempAcc = getBankAccount();
        tempAmount = tempAcc.getAmount();
        tempAmount = tempAmount - amount;
        if (tempAmount > 0) {
            tempAcc.setAmount(tempAmount);
        } else {
            throw new RuntimeException("Amount for charge too big " + tempAmount);
        }

    }
}
